export class TokenType extends String {}

export class Token {
  set _Literal(value: string) {
    this.Literal = value;
  }
  public Type: TokenType;
  public Literal: string = "";

  constructor(tt: TokenType, lit: string) {
    this.Type = tt;
    this.Literal = lit;
  }
}

export const ILLEGAL = "ILLEGAL",
  EOF = "EOF",
  IDENT = "IDENT",
  INT = "INT",
  ASSIGN = "=",
  PLUS = "+",
  COMMA = ",",
  SEMICOLON = ";",
  LPAREN = "(",
  RPAREN = ")",
  LBRACE = "{",
  RBRACE = "}",
  FUNCTION = "FUNCTION",
  LET = "LET",
  RETURN = "RETURN",
  TRUE = "TRUE",
  FALSE = "FALSE",
  IF = "IF",
  ELSE = "ELSE",
  MINUS = "-",
  BANG = "!",
  ASTERISK = "*",
  SLASH = "/",
  LT = "<",
  EQ = "==",
  NOT_EQ = "!=",
  GT = ">";

const keywords: { [k: string]: string } = {
  fn: FUNCTION,
  let: LET,
  true: TRUE,
  false: FALSE,
  if: IF,
  else: ELSE,
  return: RETURN
};

export const LookupIdent = (ident: string): TokenType => {
  // @ts-ignore
  const tok = keywords[ident];
  if (tok) {
    return tok;
  }
  return IDENT;
};

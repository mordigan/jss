import os from 'os'
import {repl} from "./repl/repl";

const main = () => {
  const {username} = os.userInfo()
  console.log(`Hello ${username}, welcome to jss.\n`)
  console.log('Type javascript in: \n')
  repl()
}

main()
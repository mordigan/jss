import * as token from "../token/token";

export class Lexer {
  public input: string = "";
  public position: number = 0;
  public readPosition: number = 0;
  public ch: string = "";

  constructor(input: string) {
    this.input = input;
    this.readChar();
  }

  private readChar(): void {
    if (this.readPosition >= this.input.length) this.ch = "\0";
    else this.ch = this.input[this.readPosition];
    this.position = this.readPosition;
    this.readPosition += 1;
  }

  public NextToken(): token.Token {
    let tok = Lexer.newToken("", "");
    this.skipWhitespace();
    switch (this.ch) {
      case "=": {
        if (this.peekChar() == "=") {
          const ch = this.ch;
          this.readChar();
          const literal = `${ch}${this.ch}`;
          tok = Lexer.newToken(token.EQ, literal);
        } else tok = Lexer.newToken(token.ASSIGN, this.ch);
        break;
      }
      case "!": {
        if (this.peekChar() == "=") {
          const ch = this.ch;
          this.readChar();
          const literal = `${ch}${this.ch}`;
          tok = Lexer.newToken(token.NOT_EQ, literal);
        } else tok = Lexer.newToken(token.BANG, this.ch);
        break;
      }
      case "-": {
        tok = Lexer.newToken(token.MINUS, this.ch);
        break;
      }
      case "/": {
        tok = Lexer.newToken(token.SLASH, this.ch);
        break;
      }
      case "+": {
        tok = Lexer.newToken(token.PLUS, this.ch);
        break;
      }
      case "*": {
        tok = Lexer.newToken(token.ASTERISK, this.ch);
        break;
      }
      case "<": {
        tok = Lexer.newToken(token.LT, this.ch);
        break;
      }
      case ">": {
        tok = Lexer.newToken(token.GT, this.ch);
        break;
      }
      case ";": {
        tok = Lexer.newToken(token.SEMICOLON, this.ch);
        break;
      }
      case "(": {
        tok = Lexer.newToken(token.LPAREN, this.ch);
        break;
      }
      case ")": {
        tok = Lexer.newToken(token.RPAREN, this.ch);
        break;
      }
      case ",": {
        tok = Lexer.newToken(token.COMMA, this.ch);
        break;
      }
      case "{": {
        tok = Lexer.newToken(token.LBRACE, this.ch);
        break;
      }
      case "}": {
        tok = Lexer.newToken(token.RBRACE, this.ch);
        break;
      }
      case "\0": {
        tok = Lexer.newToken(token.EOF, "");
        break;
      }
      default: {
        if (Lexer.isLetter(this.ch)) {
          tok.Literal = this.readIdentifier();
          tok.Type = token.LookupIdent(tok.Literal);
          return tok;
        } else if (Lexer.isDigit(this.ch)) {
          tok.Type = token.INT;
          tok.Literal = this.readNumber();
          return tok;
        } else tok = Lexer.newToken(token.ILLEGAL, this.ch);
      }
    }
    this.readChar();
    return tok;
  }

  private peekChar(): string {
    if (this.position >= this.input.length) return "\0";
    return this.input[this.readPosition];
  }

  private skipWhitespace(): void {
    while (
      this.ch == " " ||
      this.ch == "\t" ||
      this.ch == "\n" ||
      this.ch == "\r"
    )
      this.readChar();
  }

  private readIdentifier(): string {
    const position = this.position;
    while (Lexer.isLetter(this.ch)) this.readChar();
    return this.input.slice(position, this.position);
  }

  private readNumber(): string {
    const position = this.position;
    while (Lexer.isDigit(this.ch)) this.readChar();
    return this.input.slice(position, this.position);
  }

  private static isLetter(ch: string): boolean {
    return ("a" <= ch && ch <= "z") || ("A" <= ch && ch <= "Z") || ch == "_";
  }

  private static isDigit(ch: string): boolean {
    return "0" <= ch && ch <= "9";
  }

  private static newToken(tokenType: token.TokenType, ch: string): token.Token {
    return new token.Token(tokenType, ch);
  }
}

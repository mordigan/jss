import { Lexer } from "../lexer/lexer";
import {
  ASSIGN,
  ASTERISK,
  BANG,
  COMMA,
  ELSE,
  EOF,
  EQ,
  FALSE,
  FUNCTION,
  GT,
  IDENT,
  IF,
  INT,
  LBRACE,
  LET,
  LPAREN,
  LT,
  MINUS,
  NOT_EQ,
  PLUS,
  RBRACE,
  RETURN,
  RPAREN,
  SEMICOLON,
  SLASH,
  Token,
  TokenType,
  TRUE
} from "../token/token";
import { Program } from "../ast/ast";
import { IStatement } from "../interfaces/IStatement";
import { LetStatement } from "../statements/letStatement";
import { Identifier } from "../expressions/identifier";
import { ReturnStatement } from "../statements/returnStatement";
import { IExpression } from "../interfaces/IExpression";
import { ExpressionStatement } from "../expressions/expressionStatement";
import { IntegerLiteral } from "../expressions/integerLiteral";
import { PrefixExpression } from "../expressions/prefixExpression";
import { InfixExpression } from "../expressions/infixExpression";
import { BooleanExpression } from "../expressions/boolean";
import { IfExpression } from "../expressions/ifExpression";
import exp from "constants";
import { BlockStatement } from "../statements/blockStatement";
import { FunctionLiteral } from "../expressions/functionLiteral";
import { CallExpression } from "../expressions/callExpression";

type prefixParseFn = () => IExpression;
type infixParseFn = (arg: IExpression) => IExpression;

enum OPS {
  _,
  LOWEST,
  EQUALS,
  LESSGREATER,
  SUM,
  PRODUCT,
  PREFIX,
  CALL
}

const precedences: {
  // @ts-ignore
  [k: TokenType]: number;
} = {
  [EQ]: OPS.EQUALS,
  [NOT_EQ]: OPS.EQUALS,
  [LT]: OPS.LESSGREATER,
  [GT]: OPS.LESSGREATER,
  [PLUS]: OPS.SUM,
  [MINUS]: OPS.SUM,
  [SLASH]: OPS.PRODUCT,
  [ASTERISK]: OPS.PRODUCT,
  [LPAREN]: OPS.CALL
};

export class Parser {
  get infixParseFns(): {
    // @ts-ignore
    [p: TokenType]: infixParseFn;
  } {
    return this._infixParseFns;
  }

  set infixParseFns(value: {
    // @ts-ignore
    [p: TokenType]: infixParseFn;
  }) {
    this._infixParseFns = value;
  }
  get prefixParseFns(): {
    // @ts-ignore
    [p: TokenType]: prefixParseFn;
  } {
    return this._prefixParseFns;
  }

  set prefixParseFns(value: {
    // @ts-ignore
    [p: TokenType]: prefixParseFn;
  }) {
    this._prefixParseFns = value;
  }
  get curToken(): Token {
    return this._curToken;
  }

  set curToken(value: Token) {
    this._curToken = value;
  }
  get peekToken(): Token {
    return this._peekToken;
  }
  set peekToken(value: Token) {
    this._peekToken = value;
  }
  public l: Lexer;
  // @ts-ignore
  private _curToken: Token;
  // @ts-ignore
  private _peekToken: Token;
  public errors: string[];
  // @ts-ignore
  private _prefixParseFns: {
    // @ts-ignore
    [k: TokenType]: prefixParseFn;
  };

  // @ts-ignore
  private _infixParseFns: {
    // @ts-ignore
    [k: TokenType]: infixParseFn;
  };

  constructor(l: Lexer) {
    this.l = l;
    this.errors = [];
    this.prefixParseFns = {};
    this.infixParseFns = {};
    this.registerPrefix(IDENT, this.parseIdentifier);
    this.registerPrefix(INT, this.parseIntegerLiteral);
    this.registerPrefix(BANG, this.parsePrefixExpression);
    this.registerPrefix(MINUS, this.parsePrefixExpression);
    this.registerPrefix(TRUE, this.parseBoolean);
    this.registerPrefix(FALSE, this.parseBoolean);
    this.registerPrefix(LPAREN, this.parseGroupedExpression);
    this.registerPrefix(IF, this.parseIfExpression);
    this.registerPrefix(FUNCTION, this.parseFunctionLiteral);
    this.registerInfix(LPAREN, this.parseCallExpression);
    this.registerInfix(PLUS, this.parseInfixExpression);
    this.registerInfix(MINUS, this.parseInfixExpression);
    this.registerInfix(SLASH, this.parseInfixExpression);
    this.registerInfix(ASTERISK, this.parseInfixExpression);
    this.registerInfix(EQ, this.parseInfixExpression);
    this.registerInfix(NOT_EQ, this.parseInfixExpression);
    this.registerInfix(LT, this.parseInfixExpression);
    this.registerInfix(GT, this.parseInfixExpression);
    this.nextToken();
    this.nextToken();
  }

  private parseCallExpression = (fn: IExpression): IExpression => {
    const exp = new CallExpression(this.curToken, fn);
    exp.Arguments = this.parseCallArguments();
    return exp;
  };

  private parseCallArguments = (): IExpression[] => {
    let args: IExpression[] = [];
    if (this.peekTokenIs(RPAREN)) {
      this.nextToken();
      return args;
    }
    this.nextToken();
    args = [...args, this.parseExpression(OPS.LOWEST)];
    while (this.peekTokenIs(COMMA)) {
      this.nextToken();
      this.nextToken();
      args = [...args, this.parseExpression(OPS.LOWEST)];
    }
    if (!this.expectPeek(RPAREN)) return <IExpression[]>(<unknown>null);
    return args;
  };

  private parseFunctionLiteral = (): IExpression => {
    const lit = new FunctionLiteral(this.curToken);
    if (!this.expectPeek(LPAREN)) return <IExpression>(<unknown>null);
    lit.Parameters = this.parseFunctionParameters();
    if (!this.expectPeek(LBRACE)) return <IExpression>(<unknown>null);
    lit.Body = this.parseBlockStatement();
    return lit;
  };

  private parseFunctionParameters = (): Identifier[] => {
    let identifiers: Identifier[] = [];
    if (this.peekTokenIs(RPAREN)) {
      this.nextToken();
      return identifiers;
    }
    this.nextToken();
    let ident = new Identifier(this.curToken, this.curToken.Literal);
    identifiers = [...identifiers, ident];
    while (this.peekTokenIs(COMMA)) {
      this.nextToken();
      this.nextToken();
      ident = new Identifier(this.curToken, this.curToken.Literal);
      identifiers = [...identifiers, ident];
    }
    if (!this.expectPeek(RPAREN)) return <Identifier[]>(<unknown>null);
    return identifiers;
  };

  private parseIfExpression = (): IExpression => {
    const expression = new IfExpression(this.curToken);
    if (!this.expectPeek(LPAREN)) return <IExpression>(<unknown>null);
    this.nextToken();
    expression.Condition = this.parseExpression(OPS.LOWEST);
    if (!this.expectPeek(RPAREN)) return <IExpression>(<unknown>null);
    if (!this.expectPeek(LBRACE)) return <IExpression>(<unknown>null);
    expression.Consequence = this.parseBlockStatement();
    if (this.peekTokenIs(ELSE)) {
      this.nextToken();
      if (!this.expectPeek(LBRACE)) return <IExpression>(<unknown>null);
      expression.Alternative = this.parseBlockStatement();
    }
    return expression;
  };

  private parseBlockStatement = (): BlockStatement => {
    const block = new BlockStatement(this.curToken);
    this.nextToken();
    while (!this.curTokenIs(RBRACE) && !this.curTokenIs(EOF)) {
      const stmt = this.parseStatement();
      if (stmt) block.Statements = [...block.Statements, stmt];
      this.nextToken();
    }
    return block;
  };

  private parseGroupedExpression = (): IExpression => {
    this.nextToken();
    const exp = this.parseExpression(OPS.LOWEST);
    if (!this.expectPeek(RPAREN)) return <IExpression>(<unknown>null);
    return exp;
  };

  private parseBoolean = (): IExpression => {
    return new BooleanExpression(this.curToken, this.curTokenIs(TRUE));
  };

  private parseIdentifier = (): IExpression => {
    return new Identifier(this.curToken, this.curToken.Literal);
  };

  private peekPrecedence(): number {
    // @ts-ignore
    const p = precedences[this.peekToken.Type];
    if (!p) return OPS.LOWEST;
    return p;
  }

  private parseInfixExpression = (left: IExpression): IExpression => {
    const expression = new InfixExpression(
      this.curToken,
      this.curToken.Literal,
      left
    );
    const precedence = this.curPrecedence();
    this.nextToken();
    expression.Right = this.parseExpression(precedence);
    return expression;
  };

  private curPrecedence = (): number => {
    // @ts-ignore
    const p = precedences[this.curToken.Type];
    if (!p) return OPS.LOWEST;
    return p;
  };

  public Errors(): string[] {
    return this.errors;
  }

  public peekError = (t: TokenType): void => {
    const msg = `expected next token to be ${t}, got ${this.peekToken.Type} instead`;
    this.errors = [...this.errors, msg];
  };

  private nextToken = (): void => {
    this._curToken = this._peekToken;
    this._peekToken = this.l.NextToken();
  };

  private parseStatement = (): IStatement => {
    switch (this._curToken.Type) {
      case LET: {
        return this.parseLetStatement();
      }
      case RETURN: {
        return this.parseReturnStatement();
      }
      default: {
        return this.parseExpressionStatement();
      }
    }
  };

  private parseExpressionStatement = (): ExpressionStatement => {
    const stmt = new ExpressionStatement(this.curToken);
    stmt.Expression = this.parseExpression(OPS.LOWEST);
    if (this.peekTokenIs(SEMICOLON)) this.nextToken();
    return stmt;
  };

  private noPrefixParseFnError(t: TokenType): void {
    this.errors = [...this.errors, `no prefix parse function for ${t} found`];
  }

  private parseExpression = (precedence: number): IExpression => {
    // @ts-ignore
    const prefix = this.prefixParseFns[this.curToken.Type];
    if (!prefix) {
      this.noPrefixParseFnError(this.curToken.Type);
      return <IExpression>(<unknown>null);
    }
    let leftExp = prefix();
    while (!this.peekTokenIs(SEMICOLON) && precedence < this.peekPrecedence()) {
      // @ts-ignore
      const infix = this.infixParseFns[this.peekToken.Type];
      if (!infix) return leftExp;
      this.nextToken();
      leftExp = infix(leftExp);
    }
    return leftExp;
  };

  private parsePrefixExpression = (): IExpression => {
    const expression = new PrefixExpression(
      this.curToken,
      this.curToken.Literal
    );
    this.nextToken();
    expression.Right = this.parseExpression(OPS.PREFIX);
    return expression;
  };

  private parseReturnStatement = (): ReturnStatement => {
    const stmt = new ReturnStatement(this.curToken);
    this.nextToken();
    stmt.ReturnValue = this.parseExpression(OPS.LOWEST);
    if (this.peekTokenIs(SEMICOLON)) this.nextToken();
    return stmt;
  };

  private parseLetStatement = (): LetStatement => {
    const stmt = new LetStatement(this._curToken);
    if (!this.expectPeek(IDENT)) return <LetStatement>(<unknown>null);
    stmt.Name = new Identifier(this.curToken, this.curToken.Literal);
    if (!this.expectPeek(ASSIGN)) return <LetStatement>(<unknown>null);
    this.nextToken();
    stmt.Value = this.parseExpression(OPS.LOWEST);
    if (this.peekTokenIs(SEMICOLON)) this.nextToken();
    return stmt;
  };

  private curTokenIs = (t: TokenType): boolean => {
    return this._curToken.Type === t;
  };

  private peekTokenIs = (t: TokenType): boolean => {
    return this._peekToken.Type === t;
  };

  private expectPeek = (t: TokenType): boolean => {
    if (this.peekTokenIs(t)) {
      this.nextToken();
      return true;
    }
    this.peekError(t);
    return false;
  };

  public ParseProgram = (): Program => {
    const program = new Program();
    program.Statements = [];

    while (this._curToken.Type !== EOF) {
      const stmt = this.parseStatement();
      if (stmt) program.Statements = [...program.Statements, stmt];
      this.nextToken();
    }

    return program;
  };

  public registerPrefix = (t: TokenType, fn: prefixParseFn) => {
    // @ts-ignore
    this._prefixParseFns[t] = fn;
  };
  public registerInfix = (t: TokenType, fn: infixParseFn) => {
    // @ts-ignore
    this._infixParseFns[t] = fn;
  };

  public parseIntegerLiteral = (): IExpression => {
    const lit = new IntegerLiteral(this.curToken);
    const value = parseInt(this.curToken.Literal);
    if (value || value === 0) lit.Value = value;
    else {
      this.errors = [
        ...this.errors,
        `could not parse ${this.curToken.Literal} as an int`
      ];
      return <IExpression>(<unknown>null);
    }
    return lit;
  };
}

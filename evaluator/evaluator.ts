import { INode } from "../interfaces/INode";
import { IJSSObject } from "../interfaces/IJSSObject";
import { Program } from "../ast/ast";
import { IStatement } from "../interfaces/IStatement";
import { IExpression } from "../interfaces/IExpression";
import { JssInteger } from "../object/Integer";
import { JssBoolean } from "../object/boolean";
import { JssNull } from "../object/n00ll";
import { ReturnValue } from "../object/returnValue";
import { ObjectType } from "../object/objectType";
import { JssError } from "../object/error";
import { Environment } from "../object/environment";

export const TRUE = new JssBoolean(true),
  FALSE = new JssBoolean(false),
  NULL = new JssNull();

export const Eval = (node: INode, env: Environment): IJSSObject => {
  let left, right, val;
  switch (node.type) {
    case "Program":
      // @ts-ignore
      return evalProgram(node.Statements, env);
    case "ExpressionStatement":
      // @ts-ignore
      return Eval(node.Expression, env);
    case "IntegerLiteral":
      // @ts-ignore
      return new JssInteger(node.Value);
    case "BooleanExpression":
      // @ts-ignore
      return nativeBoolToBooleanObject(node.Value);
    case "PrefixExpression":
      // @ts-ignore
      right = Eval(node.Right, env);
      if (isError(right)) return right;
      // @ts-ignore
      return evalPrefixExpression(node.Operator, right);
    case "InfixExpression":
      // @ts-ignore
      left = Eval(node.Left, env);
      if (isError(left)) return left;
      // @ts-ignore
      right = Eval(node.Right, env);
      if (isError(right)) return right;
      // @ts-ignore
      return evalInfixExpression(node.Operator, left, right);
    case "BlockStatement":
      // @ts-ignore
      return node.evalBlockStatement(node.Statements, env);
    case "IfExpression":
      // @ts-ignore
      return node.evalIfExpression(env);
    case "ReturnStatement":
      // @ts-ignore
      val = Eval(node.ReturnValue, env);
      if (isError(val)) return val;
      return new ReturnValue(val);
    case "LetStatement":
      // @ts-ignore
      val = Eval(node.Value, env);
      if (isError(val)) return val;
      // @ts-ignore
      env.store = { [node.Name.Value]: val };
      break;
    case "Identifier":
      // @ts-ignore
      return node.evalIdentifier(env);
  }
  return <IJSSObject>(<unknown>null);
};

export const isError = (obj: IJSSObject): boolean => {
  if (obj) return obj.Type() == ObjectType.ERROR_OBJ;
  return false;
};

export const isTruthy = (obj: IJSSObject): boolean => {
  switch (obj) {
    case NULL:
      return false;
    case TRUE:
      return true;
    case FALSE:
      return false;
    default:
      return true;
  }
};

const evalProgram = (stmts: IStatement[], env: Environment): IJSSObject => {
  let result: IJSSObject = <IJSSObject>(<unknown>null);
  for (const s of stmts) {
    result = Eval(s, env);
    if (result) {
      switch (result.Type()) {
        case ObjectType.RETURN_VALUE_OBJ:
          // @ts-ignore
          return result.Value;
        case ObjectType.ERROR_OBJ:
          return result;
      }
    }
  }
  return result;
};

export const newError = (format: string): JssError => {
  return new JssError(format);
};

const nativeBoolToBooleanObject = (input: boolean): JssBoolean =>
  input ? TRUE : FALSE;

const evalPrefixExpression = (op: string, right: IJSSObject): IJSSObject => {
  switch (op) {
    case "!":
      return evalBangOperatorExpression(right);
    case "-":
      return evalMinusPrefixOperatorExpression(right);
    default:
      return newError(`unknown operator ${op} ${right.Type()}`);
  }
};

const evalBangOperatorExpression = (right: IJSSObject) => {
  switch (right) {
    case TRUE:
      return FALSE;
    case FALSE:
      return TRUE;
    case NULL:
      return TRUE;
    default:
      return FALSE;
  }
};

const evalMinusPrefixOperatorExpression = (right: IJSSObject) => {
  if (right.Type() != ObjectType.INTEGER_OBJ)
    return newError(`unknown operator: -${right.Type()}`);
  // @ts-ignore
  const value = right.Value;
  return new JssInteger(-value);
};

const evalInfixExpression = (
  op: string,
  left: IJSSObject,
  right: IJSSObject
): IJSSObject => {
  if (left.Type() !== right.Type())
    return newError(
      `Ебанный рот этого казино нахуй, ты кто такой что-бы это сделать?`
    );
  if (
    left.Type() === ObjectType.INTEGER_OBJ &&
    right.Type() === ObjectType.INTEGER_OBJ
  )
    return evalIntegerInfixExpression(op, left, right);
  if (op === "==") return nativeBoolToBooleanObject(left == right);
  if (op == "!=") return nativeBoolToBooleanObject(left != right);
  return newError(`unknown operator: ${left.Type()} ${op} ${right.Type()}`);
};

const evalIntegerInfixExpression = (
  op: string,
  l: IJSSObject,
  r: IJSSObject
): IJSSObject => {
  // @ts-ignore
  const lVal = l.Value;
  // @ts-ignore
  const rVal = r.Value;
  switch (op) {
    case "+":
      return new JssInteger(lVal + rVal);
    case "-":
      return new JssInteger(lVal - rVal);
    case "*":
      return new JssInteger(lVal * rVal);
    case "/":
      return new JssInteger(lVal / rVal);
    case "<":
      return nativeBoolToBooleanObject(lVal < rVal);
    case ">":
      return nativeBoolToBooleanObject(lVal > rVal);
    case "==":
      return nativeBoolToBooleanObject(lVal === rVal);
    case "!=":
      return nativeBoolToBooleanObject(lVal != rVal);
    default:
      return newError(`unknown operator: ${l.Type()} ${op} ${r.Type()}`);
  }
};

import { INode } from "./INode";

export interface IStatement extends INode {
  statementNode(): void;
}

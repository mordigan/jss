import { ObjectType } from "../object/objectType";

export interface IJSSObject {
  Type(): ObjectType;
  Inspect(): string;
}

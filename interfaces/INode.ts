export interface INode {
  type: string;
  TokenLiteral(): string;
  String(): string;
}

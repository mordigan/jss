import { INode } from "./INode";

export interface IExpression extends INode {
  expressionNode(): void;
}

import * as token from "../token/token";
import * as lexer from "../lexer/lexer";
// @ts-ignore
import readline from "readline-sync";
import { Parser } from "../parser/parser";
import { Eval } from "../evaluator/evaluator";
import { Environment } from "../object/environment";

const PROMPT = "> ";

const printParserErrors = (errors: string[]): void => {
  console.log("parse errors: ");
  for (const e of errors) console.log(`\t${e}`);
};

export const repl = (): void => {
  const env = new Environment();
  while (true) {
    process.stdout.write(PROMPT);
    const input: string = readline.question("");
    if (!input) {
      return;
    }
    const l = new lexer.Lexer(input);
    const p = new Parser(l);
    const program = p.ParseProgram();
    if (p.Errors().length) {
      printParserErrors(p.Errors());
      continue;
    }
    const evaluated = Eval(program, env);
    if (evaluated) console.log(evaluated.Inspect());
  }
};

import { IExpression } from "../interfaces/IExpression";
import { Token } from "../token/token";
import { BlockStatement } from "../statements/blockStatement";
import { IJSSObject } from "../interfaces/IJSSObject";
import { Eval, isError, isTruthy, NULL } from "../evaluator/evaluator";
import { Environment } from "../object/environment";

export class IfExpression implements IExpression {
  type = "IfExpression";
  get Alternative(): BlockStatement {
    return this._Alternative;
  }

  set Alternative(value: BlockStatement) {
    this._Alternative = value;
  }
  get Consequence(): BlockStatement {
    return this._Consequence;
  }

  set Consequence(value: BlockStatement) {
    this._Consequence = value;
  }
  get Condition(): IExpression {
    return this._Condition;
  }

  set Condition(value: IExpression) {
    this._Condition = value;
  }
  get Token(): Token {
    return this._Token;
  }

  set Token(value: Token) {
    this._Token = value;
  }
  // @ts-ignore
  private _Token: Token;
  // @ts-ignore
  private _Condition: IExpression;
  // @ts-ignore
  private _Consequence: BlockStatement;
  // @ts-ignore
  private _Alternative: BlockStatement;
  String(): string {
    let out: Buffer = Buffer.from("");
    out = Buffer.concat([out, Buffer.from("if")]);
    out = Buffer.concat([out, Buffer.from(this._Condition.String())]);
    out = Buffer.concat([out, Buffer.from(" ")]);
    out = Buffer.concat([out, Buffer.from(this._Consequence.String())]);
    if (this._Alternative) {
      out = Buffer.concat([out, Buffer.from("else ")]);
      out = Buffer.concat([out, Buffer.from(this._Alternative.String())]);
    }
    return out.toString();
  }

  TokenLiteral(): string {
    return this._Token.Literal;
  }

  expressionNode(): void {}

  constructor(t: Token) {
    this.Token = t;
  }

  public evalIfExpression = (env: Environment): IJSSObject => {
    const condition = Eval(this.Condition, env);
    if (isError(condition)) return condition;
    if (isTruthy(condition)) return Eval(this.Consequence, env);
    else if (this.Alternative) return Eval(this.Alternative, env);
    else return NULL;
  };
}

import { IExpression } from "../interfaces/IExpression";
import { Token } from "../token/token";
import { Identifier } from "./identifier";
import { BlockStatement } from "../statements/blockStatement";

export class FunctionLiteral implements IExpression {
  type = "FunctionLiteral";
  get Body(): BlockStatement {
    return this._Body;
  }

  set Body(value: BlockStatement) {
    this._Body = value;
  }
  get Parameters(): Identifier[] {
    return this._Parameters;
  }

  set Parameters(value: Identifier[]) {
    this._Parameters = value;
  }
  get Token(): Token {
    return this._Token;
  }

  set Token(value: Token) {
    this._Token = value;
  }
  // @ts-ignore
  private _Token: Token;
  // @ts-ignore
  private _Parameters: Identifier[];
  // @ts-ignore
  private _Body: BlockStatement;
  String(): string {
    let out: Buffer = Buffer.from("");
    let params: string[] = [];
    for (const p of this._Parameters) params = [...params, p.String()];
    out = Buffer.concat([out, Buffer.from(this.TokenLiteral())]);
    out = Buffer.concat([out, Buffer.from("(")]);
    out = Buffer.concat([out, Buffer.from(params.join(", "))]);
    out = Buffer.concat([out, Buffer.from(")")]);
    out = Buffer.concat([out, Buffer.from(this._Body.String())]);
    return out.toString();
  }

  TokenLiteral(): string {
    return this._Token.Literal;
  }

  expressionNode(): void {}

  constructor(t: Token) {
    this.Token = t;
  }
}

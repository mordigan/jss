import { IExpression } from "../interfaces/IExpression";
import { Token } from "../token/token";

export class IntegerLiteral implements IExpression {
  type = "IntegerLiteral";
  get Value(): number {
    return this._Value;
  }

  set Value(value: number) {
    this._Value = value;
  }
  get Token(): Token {
    return this._Token;
  }

  set Token(value: Token) {
    this._Token = value;
  }
  // @ts-ignore
  private _Token: Token;
  // @ts-ignore
  private _Value: number;
  String(): string {
    return this._Token.Literal;
  }

  TokenLiteral(): string {
    return this._Token.Literal;
  }

  expressionNode(): void {}

  constructor(t: Token) {
    this.Token = t;
  }
}

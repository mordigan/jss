import { IStatement } from "../interfaces/IStatement";
import { Token } from "../token/token";
import { IExpression } from "../interfaces/IExpression";

export class ExpressionStatement implements IStatement {
  type = "ExpressionStatement";
  get Expression(): IExpression {
    return this._Expression;
  }

  set Expression(value: IExpression) {
    this._Expression = value;
  }
  get Token(): Token {
    return this._Token;
  }

  set Token(value: Token) {
    this._Token = value;
  }
  // @ts-ignore
  private _Token: Token;
  // @ts-ignore
  private _Expression: IExpression;

  constructor(t: Token) {
    this.Token = t;
  }

  TokenLiteral(): string {
    return this._Token.Literal;
  }

  statementNode(): void {}

  String(): string {
    if (this.Expression) return this.Expression.String();
    return "";
  }
}

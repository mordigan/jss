import { IExpression } from "../interfaces/IExpression";
import { Token } from "../token/token";

export class BooleanExpression implements IExpression {
  get Value(): boolean {
    return this._Value;
  }

  set Value(value: boolean) {
    this._Value = value;
  }
  get Token(): Token {
    return this._Token;
  }

  set Token(value: Token) {
    this._Token = value;
  }
  // @ts-ignore
  private _Token: Token;
  // @ts-ignore
  private _Value: boolean;
  String(): string {
    return this.TokenLiteral();
  }

  TokenLiteral(): string {
    return this._Token.Literal;
  }

  expressionNode(): void {}

  constructor(t: Token, v: boolean) {
    this.Token = t;
    this.Value = v;
  }

  type = "BooleanExpression";
}

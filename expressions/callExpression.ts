import { IExpression } from "../interfaces/IExpression";
import { Token } from "../token/token";

export class CallExpression implements IExpression {
  type = "CallExpression";
  get Arguments(): IExpression[] {
    return this._Arguments;
  }

  set Arguments(value: IExpression[]) {
    this._Arguments = value;
  }
  get Function(): IExpression {
    return this._Function;
  }

  set Function(value: IExpression) {
    this._Function = value;
  }
  get Token(): Token {
    return this._Token;
  }

  set Token(value: Token) {
    this._Token = value;
  }
  // @ts-ignore
  private _Token: Token;
  // @ts-ignore
  private _Function: IExpression;
  // @ts-ignore
  private _Arguments: IExpression[];
  String(): string {
    let out: Buffer = Buffer.from("");
    let args: string[] = [];
    for (const a of this._Arguments) args = [...args, a.String()];
    out = Buffer.concat([out, Buffer.from(this._Function.String())]);
    out = Buffer.concat([out, Buffer.from("(")]);
    out = Buffer.concat([out, Buffer.from(args.join(", "))]);
    out = Buffer.concat([out, Buffer.from(")")]);
    return out.toString();
  }

  TokenLiteral(): string {
    return this._Token.Literal;
  }

  expressionNode(): void {}

  constructor(t: Token, fn: IExpression) {
    this.Token = t;
    this.Function = fn;
  }
}

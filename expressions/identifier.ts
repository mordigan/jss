import { IExpression } from "../interfaces/IExpression";
import { Token } from "../token/token";
import { Environment } from "../object/environment";
import { IJSSObject } from "../interfaces/IJSSObject";
import { newError } from "../evaluator/evaluator";

export class Identifier implements IExpression {
  public Token: Token;
  public Value: string = "";

  constructor(tok: Token, val: string) {
    this.Token = tok;
    this.Value = val;
  }

  TokenLiteral(): string {
    return this.Token.Literal;
  }

  expressionNode(): void {}

  String(): string {
    return this.Value;
  }

  type = "Identifier";

  public evalIdentifier = (env: Environment): IJSSObject => {
    const val = env.Get(this.Value);
    if (!val) return newError(`identifier not found: ${this.Value}`);
    return val;
  };
}

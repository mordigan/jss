import { IExpression } from "../interfaces/IExpression";
import { Token } from "../token/token";

export class InfixExpression implements IExpression {
  type = "InfixExpression";
  get Right(): IExpression {
    return this._Right;
  }

  set Right(value: IExpression) {
    this._Right = value;
  }
  get Operator(): string {
    return this._Operator;
  }

  set Operator(value: string) {
    this._Operator = value;
  }
  get Left(): IExpression {
    return this._Left;
  }

  set Left(value: IExpression) {
    this._Left = value;
  }
  get Token(): Token {
    return this._Token;
  }

  set Token(value: Token) {
    this._Token = value;
  }
  // @ts-ignore
  private _Token: Token;
  // @ts-ignore
  private _Left: IExpression;
  // @ts-ignore
  private _Operator: string;
  // @ts-ignore
  private _Right: IExpression;
  String(): string {
    let out: Buffer = Buffer.from("");
    out = Buffer.concat([out, Buffer.from("(")]);
    out = Buffer.concat([out, Buffer.from(this._Left.String())]);
    out = Buffer.concat([out, Buffer.from(` ${this._Operator} `)]);
    out = Buffer.concat([out, Buffer.from(this._Right.String())]);
    out = Buffer.concat([out, Buffer.from(")")]);
    return out.toString();
  }

  TokenLiteral(): string {
    return this._Token.Literal;
  }

  expressionNode(): void {}

  constructor(t: Token, op: string, left: IExpression) {
    this.Token = t;
    this.Operator = op;
    this.Left = left;
  }
}

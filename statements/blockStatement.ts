import { IStatement } from "../interfaces/IStatement";
import { Token } from "../token/token";
import { IJSSObject } from "../interfaces/IJSSObject";
import { ObjectType } from "../object/objectType";
import { Eval } from "../evaluator/evaluator";
import { Environment } from "../object/environment";

export class BlockStatement implements IStatement {
  type = "BlockStatement";
  get Statements(): IStatement[] {
    return this._Statements;
  }

  set Statements(value: IStatement[]) {
    this._Statements = value;
  }
  get Token(): Token {
    return this._Token;
  }

  set Token(value: Token) {
    this._Token = value;
  }
  // @ts-ignore
  private _Token: Token;
  // @ts-ignore
  private _Statements: IStatement[];
  String(): string {
    let out: Buffer = Buffer.from("");
    for (const s of this._Statements)
      out = Buffer.concat([out, Buffer.from(s.String())]);
    return out.toString();
  }

  TokenLiteral(): string {
    return this._Token.Literal;
  }

  statementNode(): void {}

  constructor(t: Token) {
    this.Token = t;
    this.Statements = [];
  }

  public evalBlockStatement = (env: Environment): IJSSObject => {
    let result: IJSSObject = <IJSSObject>(<unknown>null);
    for (const s of this.Statements) {
      result = Eval(s, env);
      if (result) {
        const rt = result.Type();
        if (rt == ObjectType.RETURN_VALUE_OBJ || rt == ObjectType.ERROR_OBJ)
          return result;
      }
    }
    return result;
  };
}

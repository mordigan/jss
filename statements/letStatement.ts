import { IStatement } from "../interfaces/IStatement";
import { Token } from "../token/token";
import { Identifier } from "../expressions/identifier";
import { IExpression } from "../interfaces/IExpression";

export class LetStatement implements IStatement {
  type = "LetStatement";
  get Value(): IExpression {
    return this._Value;
  }

  set Value(value: IExpression) {
    this._Value = value;
  }
  get Name(): Identifier {
    return this._Name;
  }

  set Name(value: Identifier) {
    this._Name = value;
  }
  public Token: Token;
  // @ts-ignore
  private _Name: Identifier;
  // @ts-ignore
  private _Value: IExpression;
  constructor(token: Token) {
    this.Token = token;
  }
  TokenLiteral(): string {
    return this.Token.Literal;
  }

  statementNode(): void {}

  String(): string {
    let out: Buffer = Buffer.from("");
    out = Buffer.concat([out, Buffer.from(`${this.TokenLiteral()} `)]);
    out = Buffer.concat([out, Buffer.from(this.Name.String())]);
    out = Buffer.concat([out, Buffer.from(" = ")]);
    if (this.Value)
      out = Buffer.concat([out, Buffer.from(this.Value.String())]);
    out = Buffer.concat([out, Buffer.from(";")]);
    return out.toString();
  }
}

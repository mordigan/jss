import { IStatement } from "../interfaces/IStatement";
import { Token } from "../token/token";
import { IExpression } from "../interfaces/IExpression";

export class ReturnStatement implements IStatement {
  type = "ReturnStatement";
  get ReturnValue(): IExpression {
    return this._ReturnValue;
  }

  set ReturnValue(value: IExpression) {
    this._ReturnValue = value;
  }
  get Token(): Token {
    return this._Token;
  }

  set Token(value: Token) {
    this._Token = value;
  }
  // @ts-ignore
  private _Token: Token;
  // @ts-ignore
  private _ReturnValue: IExpression;

  constructor(t: Token) {
    this.Token = t;
  }

  TokenLiteral(): string {
    return this._Token.Literal;
  }

  statementNode(): void {}

  String(): string {
    let out: Buffer = Buffer.from("");
    out = Buffer.concat([out, Buffer.from(`${this.TokenLiteral()} `)]);
    if (this.ReturnValue)
      out = Buffer.concat([out, Buffer.from(this.ReturnValue.String())]);
    out = Buffer.concat([out, Buffer.from(";")]);
    return out.toString();
  }
}

import { IJSSObject } from "../interfaces/IJSSObject";
import { ObjectType } from "./objectType";

export class JssBoolean implements IJSSObject {
  constructor(Value: boolean) {
    this.Value = Value;
  }

  get Value(): boolean {
    return this._Value;
  }

  set Value(value: boolean) {
    this._Value = value;
  }
  // @ts-ignore
  private _Value: boolean;
  Inspect(): string {
    return `${this._Value}`;
  }

  Type(): ObjectType {
    return ObjectType.BOOLEAN_OBJ;
  }
}

import { IJSSObject } from "../interfaces/IJSSObject";
import { ObjectType } from "./objectType";

export class JssInteger implements IJSSObject {
  get Value(): number {
    return this._Value;
  }

  set Value(value: number) {
    this._Value = value;
  }
  // @ts-ignore
  private _Value: number;
  Inspect(): string {
    return `${this._Value}`;
  }

  Type(): ObjectType {
    return ObjectType.INTEGER_OBJ;
  }

  constructor(v: number) {
    this.Value = v;
  }
}

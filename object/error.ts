import { IJSSObject } from "../interfaces/IJSSObject";
import { ObjectType } from "./objectType";

export class JssError implements IJSSObject {
  public Message: string;
  Inspect(): string {
    return `ERROR: ${this.Message}`;
  }

  Type(): ObjectType {
    return ObjectType.ERROR_OBJ;
  }

  constructor(m: string) {
    this.Message = m;
  }
}

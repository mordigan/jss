import { IJSSObject } from "../interfaces/IJSSObject";
import { ObjectType } from "./objectType";

export class JssNull implements IJSSObject {
  Inspect(): string {
    return "null";
  }

  Type(): ObjectType {
    return ObjectType.NULL_OBJ;
  }
}

import { IJSSObject } from "../interfaces/IJSSObject";

export class Environment {
  get store(): { [p: string]: IJSSObject } {
    return this._store;
  }

  public Get(k: string) {
    return this._store[k];
  }

  set store(value: { [p: string]: IJSSObject }) {
    this._store = { ...this._store, ...value };
  }
  private _store: { [k: string]: IJSSObject };
  constructor() {
    this._store = {};
  }
}

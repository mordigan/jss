import { IJSSObject } from "../interfaces/IJSSObject";
import { ObjectType } from "./objectType";

export class ReturnValue implements IJSSObject {
  get Value(): IJSSObject {
    return this._Value;
  }

  set Value(value: IJSSObject) {
    this._Value = value;
  }
  // @ts-ignore
  private _Value: IJSSObject;
  Inspect(): string {
    return this._Value.Inspect();
  }

  Type(): ObjectType {
    return ObjectType.RETURN_VALUE_OBJ;
  }

  constructor(v: IJSSObject) {
    this.Value = v;
  }
}

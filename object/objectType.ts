export class ObjectType extends String {
  static INTEGER_OBJ = "INTEGER";
  static BOOLEAN_OBJ = "BOOLEAN";
  static NULL_OBJ = "NULL";
  static RETURN_VALUE_OBJ = "RETURN_VALUE";
  static ERROR_OBJ = "ERROR";
}

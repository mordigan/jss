import { IStatement } from "../interfaces/IStatement";
import { INode } from "../interfaces/INode";

export class Program implements INode {
  public Statements: IStatement[] = [];

  TokenLiteral(): string {
    if (this.Statements.length > 0) return this.Statements[0].TokenLiteral();
    return "";
  }
  String(): string {
    let out: Buffer = Buffer.from("");
    for (const s of this.Statements)
      out = Buffer.concat([out, Buffer.from(s.String())]);

    return out.toString();
  }

  type = "Program";
}
